﻿using UnityEngine;
using System.Collections;

public class missileRun : MonoBehaviour
{
	
	public float speed = 30.0f;
	public int duration;
	float timeLeft;

    public Vector3 movement;
    private Rigidbody rigid;

	// Use this for initialization
	void Start ()
	{
		//
        timeLeft = duration;
        rigid = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		//transform.Translate (transform.InverseTransformDirection (transform.up * temp));
		timeLeft -= Time.deltaTime;
		if (timeLeft <= 0)
			Destroy (gameObject);

        movement = transform.up * speed;
        rigid.velocity = movement;
	}
}
