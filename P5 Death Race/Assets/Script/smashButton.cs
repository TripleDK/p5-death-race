﻿using UnityEngine;
using System.Collections;

public class smashButton : MonoBehaviour
{


	public GameObject leftHand;
	public GameObject rightHand;
	public float interactRange;
	public float minSpeed;
	Vector3 leftHandprev;
	Vector3 leftHandnow;
	Vector3 rightHandprev;
	Vector3 rightHandnow;

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	public void Update ()
	{
		leftHandnow = leftHand.transform.position;
		rightHandnow = rightHand.transform.position;


		//Debug.Log ("Lefthand speed = " + (leftHandnow - leftHandprev).magnitude);
		//	Debug.Log ("Righthand speed = " + (rightHandnow - rightHandprev).magnitude);

		if ((transform.position - leftHand.transform.position).magnitude < interactRange && (leftHandnow - leftHandprev).magnitude > minSpeed) {
			interaction ();
		}
		if ((transform.position - rightHand.transform.position).magnitude < interactRange && (rightHandnow - rightHandprev).magnitude > minSpeed) {
			interaction ();
		}



		leftHandprev = leftHand.transform.position;
		rightHandprev = rightHand.transform.position;
	}

	public virtual void interaction ()
	{
		
	}
}
