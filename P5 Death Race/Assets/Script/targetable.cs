﻿using UnityEngine;
using System.Collections;

public class targetable : MonoBehaviour
{

	public GameObject explosion;

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	void OnCollisionEnter (Collision collision)
	{
		//Debug.Log ("Target collided with something");
		//	Debug.Log ("Target collided with " + collision.gameObject.tag);
		if (collision.gameObject.tag == "missile") {
			Debug.Log ("There is supposed to be an Earth-shattering kaboom!");
			Destroy (collision.gameObject);
			GameObject tempExplosion = Instantiate (explosion, transform.position, Quaternion.identity) as GameObject;
			tempExplosion.transform.RotateAround (tempExplosion.transform.position, transform.right, -90);
			Destroy (gameObject);
		}
	}
}
