﻿using UnityEngine;
using System.Collections;

public class CarControls : MonoBehaviour
{

	public GameObject stick;
	public GameObject stickEnd;
	public GameObject wheel;
	public GameObject turboButton;
	public GameObject missilePrefab;
	public Vector3 positionAdjustment;
	turboButton turboScript;
	Vector3 turboButtonOrigin;
	public float stopThreshold = 0.001f;
	public GameObject boolet;
	public float machineGunCD = 0.1f;
	float machineGunCool;
	float tempSpeed;
	private Vector3 movement;
	private Rigidbody rigid;
	public float speedMultiplier = 0.5f;

	// Use this for initialization
	void Start ()
	{
		turboScript = turboButton.GetComponent<turboButton> ();
		rigid = GetComponent<Rigidbody> ();
		turboButtonOrigin = turboButton.transform.localPosition;
		machineGunCool = 0;
	}
	
	// Update is called once per frame
	void Update ()
	{
		tempSpeed = stick.GetComponent<leverGrab> ().value * Time.deltaTime;
		if (turboScript.boostOn) {
			tempSpeed *= turboScript.speedBoost;
			turboScript.timeLeft -= Time.deltaTime;
			turboButton.transform.localPosition = turboButtonOrigin - new Vector3 (0, 0.15f, 0);
		}
		if (turboScript.timeLeft <= 0 && turboScript.boostOn) {
			turboScript.boostOn = false;
			turboButton.transform.localPosition = turboButtonOrigin;
		}

		if (Mathf.Abs (tempSpeed) < stopThreshold) {
			tempSpeed = 0.0f;
		}

		// Move car
		if (ControlSwitch.Instance.useHydra == false) {
			transform.Translate (transform.InverseTransformDirection (transform.forward) * tempSpeed * speedMultiplier);
			// Debug.Log ("Accelerate at a leveled pace: " + transform.forward * (stick.transform.eulerAngles.x - 270) * Time.deltaTime);

			// Turn car
			transform.RotateAround (transform.position, transform.up, tempSpeed * wheel.GetComponent<steeringWheelGrab> ().servoSide * wheel.GetComponent<steeringWheelGrab> ().realAngle);
			// Debug.Log ("Turning car by: " + wheel.GetComponent<steeringWheelGrab> ().turnSide * (wheel.transform.localEulerAngles.x - 90) / 5);

			//Debug.Log ("Turning: " + wheel.GetComponent<steeringWheelGrab> ().turnSide + ", accelerating: " + stick.GetComponent<leverGrab> ().value);
			//	Debug.Log ("Turn nigga: " + wheel.transform.localEulerAngles.x);

			//movement = transform.InverseTransformDirection(transform.forward) * tempSpeed * 7;
			//rigid.AddForce(movement);

			if (SixenseInput.Controllers [0].GetButton (SixenseButtons.THREE) == true) {
				Debug.Log ("Left three!");
				
				// Move car
				//	transform.Translate (transform.InverseTransformDirection (transform.forward) * 10);
				// Debug.Log ("Accelerate at a leveled pace: " + transform.forward * (stick.transform.eulerAngles.x - 270) * Time.deltaTime);
				
				//Fire boolets
				if (machineGunCool <= 0) {
					GameObject newBoolet = Instantiate (boolet, transform.TransformPoint (positionAdjustment), transform.rotation) as GameObject;
					newBoolet.transform.RotateAround (newBoolet.transform.position, transform.right, 90);
					newBoolet = Instantiate (boolet, transform.TransformPoint (positionAdjustment + new Vector3 (0.3f, 0f, 0f)), transform.rotation) as GameObject;
					newBoolet.transform.RotateAround (newBoolet.transform.position, transform.right, 90);
					machineGunCool = machineGunCD;
				}
				
			}
			
			if (SixenseInput.Controllers [1].GetButtonDown (SixenseButtons.FOUR) == true) {
				Debug.Log ("Right four!");
				//Fire boolets
				if (machineGunCool <= 0) {
					GameObject newBoolet = Instantiate (boolet, transform.TransformPoint (positionAdjustment + new Vector3 (0.3f, 0f, 0f)), transform.rotation) as GameObject;
					newBoolet.transform.RotateAround (newBoolet.transform.position, transform.right, 90);
					newBoolet = Instantiate (boolet, transform.TransformPoint (positionAdjustment + new Vector3 (0.3f, 0f, 0f)), transform.rotation) as GameObject;
					newBoolet.transform.RotateAround (newBoolet.transform.position, transform.right, 90);
					machineGunCool = machineGunCD;
				}
				
			}
		} else {
			if (SixenseInput.Controllers [0].GetButton (SixenseButtons.ONE) == true) {
				Debug.Log ("Left one!");

				// Move car
				//    transform.Translate(transform.InverseTransformDirection(transform.forward) * 1);
				// Debug.Log ("Accelerate at a leveled pace: " + transform.forward * (stick.transform.eulerAngles.x - 270) * Time.deltaTime);

			}

			if (SixenseInput.Controllers [0].GetButton (SixenseButtons.TWO) == true) {
				Debug.Log ("Left two!");

				// Move car
				//      transform.Translate(transform.InverseTransformDirection(transform.forward) * -1);
				// Debug.Log ("Accelerate at a leveled pace: " + transform.forward * (stick.transform.eulerAngles.x - 270) * Time.deltaTime);

			}

			if (SixenseInput.Controllers [0].GetButton (SixenseButtons.THREE) == true) {
				//	Debug.Log ("Left three!");

				// Move car
				//	transform.Translate (transform.InverseTransformDirection (transform.forward) * 10);
				// Debug.Log ("Accelerate at a leveled pace: " + transform.forward * (stick.transform.eulerAngles.x - 270) * Time.deltaTime);

				//Fire boolets
				if (machineGunCool <= 0) {
					GameObject newBoolet = Instantiate (boolet, transform.TransformPoint (positionAdjustment), transform.rotation) as GameObject;
					newBoolet.transform.RotateAround (newBoolet.transform.position, transform.right, 90);
					machineGunCool = machineGunCD;
				}

			}

			if (SixenseInput.Controllers [1].GetButtonDown (SixenseButtons.FOUR) == true) {
				//	Debug.Log ("Right four!");

				//Fire boolets
				if (machineGunCool <= 0) {
					GameObject newBoolet = Instantiate (boolet, transform.TransformPoint (positionAdjustment + new Vector3 (0.3f, 0, 0)), transform.rotation) as GameObject;
					newBoolet.transform.RotateAround (newBoolet.transform.position, transform.right, 90);
					machineGunCool = machineGunCD;
				}
			}

			if (SixenseInput.Controllers [0].GetButtonDown (SixenseButtons.BUMPER) == true) {
				Debug.Log ("Left bumper!");

				GameObject newMissile = Instantiate (missilePrefab, gameObject.transform.TransformPoint (positionAdjustment), transform.rotation) as GameObject;

				newMissile.transform.RotateAround (newMissile.transform.position, transform.right, 90);

			}

			transform.Translate (transform.InverseTransformDirection (transform.forward) * SixenseInput.Controllers [0].Trigger * (-1) * speedMultiplier);
			transform.Translate (transform.InverseTransformDirection (transform.forward) * SixenseInput.Controllers [1].Trigger * speedMultiplier);
			// Debug.Log ("Accelerate at a leveled pace: " + transform.forward * (stick.transform.eulerAngles.x - 270) * Time.deltaTime);	;

		

		}
		if (machineGunCool > 0) {
			machineGunCool -= Time.deltaTime;
			//Debug.Log (machineGunCool); iajsidj
		}
	}
    
}
