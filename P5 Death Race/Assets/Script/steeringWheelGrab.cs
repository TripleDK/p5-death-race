using UnityEngine;
using System.Collections;

public class steeringWheelGrab : MonoBehaviour
{

	public GameObject wheelCollider1;
	public GameObject wheelCollider2;
	public GameObject wheelCollider3;
	public GameObject wheelCollider4;
	public GameObject wheelCollider5;
	public GameObject wheelCollider6;
	Collider wheelCollidercol1;
	Collider wheelCollidercol2;
	Collider wheelCollidercol3;
	Collider wheelCollidercol4;
	Collider wheelCollidercol5;
	Collider wheelCollidercol6;
	public GameObject leftHand;
	public GameObject rightHand;
	public GameObject wheelCenterL;
	public GameObject wheelCenterR;
	public GameObject wheelCenterTop;
	public int turnSide;
	public int servoSide;
	public int twoHandsThresh;
	public float realAngle;
	public float interactRange;
	public float grabThresh;
	Collider leftHandCollider;
	Collider rightHandCollider;
	bool leftHandOn = false;
	bool rightHandOn = false;
	Vector3 oldPointL = new Vector3 (0, 0, 0);
	Vector3 newPointL = new Vector3 (0, 0, 0);
	Vector3 oldPointR = new Vector3 (0, 0, 0);
	Vector3 newPointR = new Vector3 (0, 0, 0);

	// Use this for initialization
	void Start ()
	{
		leftHandCollider = leftHand.GetComponent<Collider> ();
		rightHandCollider = rightHand.GetComponent<Collider> ();
		wheelCollidercol1 = wheelCollider1.GetComponent<Collider> ();
		wheelCollidercol2 = wheelCollider2.GetComponent<Collider> ();
		wheelCollidercol3 = wheelCollider3.GetComponent<Collider> ();
		wheelCollidercol4 = wheelCollider4.GetComponent<Collider> ();
		wheelCollidercol5 = wheelCollider5.GetComponent<Collider> ();
		wheelCollidercol6 = wheelCollider6.GetComponent<Collider> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		realAngle = transform.localEulerAngles.z;
		if (transform.localEulerAngles.z > 180)
			realAngle -= 360;

		//right hand
		if (SixenseInput.Controllers [1].Trigger > grabThresh && (
			rightHandCollider.bounds.Intersects (wheelCollidercol1.bounds)
			|| rightHandCollider.bounds.Intersects (wheelCollidercol2.bounds) 
			|| rightHandCollider.bounds.Intersects (wheelCollidercol3.bounds) 
			|| rightHandCollider.bounds.Intersects (wheelCollidercol4.bounds) 
			|| rightHandCollider.bounds.Intersects (wheelCollidercol5.bounds) 
			|| rightHandCollider.bounds.Intersects (wheelCollidercol6.bounds))) {

			rightHandOn = true;
			//	Debug.Log ("Right hand on wheel, pointing towards " + new Vector3 (rightHand.transform.position.x, rightHand.transform.position.y, wheelCenterL.transform.position.z));
		} else {
			rightHandOn = false;
		}
		//left hand
		if (SixenseInput.Controllers [0].Trigger > grabThresh && (
			leftHandCollider.bounds.Intersects (wheelCollidercol1.bounds)
			|| leftHandCollider.bounds.Intersects (wheelCollidercol2.bounds) 
			|| leftHandCollider.bounds.Intersects (wheelCollidercol3.bounds) 
			|| leftHandCollider.bounds.Intersects (wheelCollidercol4.bounds) 
			|| leftHandCollider.bounds.Intersects (wheelCollidercol5.bounds) 
			|| leftHandCollider.bounds.Intersects (wheelCollidercol6.bounds))) {

			leftHandOn = true;
			//	Debug.Log ("Left hand on wheel, pointing towards " + new Vector3 (leftHand.transform.position.x, leftHand.transform.position.y, wheelCenterL.transform.position.z));
		} else {
			leftHandOn = false;
		}

		//rotate wheel
		if (leftHandOn && rightHandOn) {
			//begge hænder på rattet 
			
			Vector3 newPointL = new Vector3 ((transform.InverseTransformPoint (leftHand.transform.position)).x, (transform.InverseTransformPoint (leftHand.transform.position)).y, (transform.InverseTransformPoint (wheelCenterL.transform.position)).z);
			wheelCenterL.transform.LookAt (transform.TransformPoint (newPointL));

			Vector3 newPointR = new Vector3 (transform.InverseTransformPoint (rightHand.transform.position).x, transform.InverseTransformPoint (rightHand.transform.position).y, transform.InverseTransformPoint (wheelCenterR.transform.position).z);
			wheelCenterR.transform.LookAt (transform.TransformPoint (newPointR));


			float leftAngle = Vector3.Angle (oldPointL - transform.InverseTransformPoint (wheelCenterL.transform.position), newPointL - transform.InverseTransformPoint (wheelCenterL.transform.position));
		
			float rightAngle = Vector3.Angle (oldPointR - transform.InverseTransformPoint (wheelCenterR.transform.position), newPointR - transform.InverseTransformPoint (wheelCenterR.transform.position));

			if ((Vector3.Cross (oldPointL - transform.InverseTransformPoint (wheelCenterL.transform.position), newPointL - transform.InverseTransformPoint (wheelCenterL.transform.position))).z > 0) {
				turnSide = 1;
			} else { 
				turnSide = -1;
			}

			leftAngle *= turnSide;
			if ((Vector3.Cross (oldPointR - transform.InverseTransformPoint (wheelCenterR.transform.position), newPointR - transform.InverseTransformPoint (wheelCenterR.transform.position))).z > 0) {
				turnSide = 1;
			} else { 
				turnSide = -1;
			}

			rightAngle *= turnSide;

			if (leftAngle > 0 && rightAngle > 0 || leftAngle < 0 && rightAngle < 0) {
				if (Mathf.Abs (leftAngle - rightAngle) < twoHandsThresh) {
					transform.RotateAround (transform.position, transform.forward, (leftAngle + rightAngle) / 2);
				} else {
					if (Mathf.Min (Mathf.Abs (leftAngle), Mathf.Abs (rightAngle)) == Mathf.Abs (leftAngle))
						transform.RotateAround (transform.position, transform.forward, leftAngle);
					else 
						transform.RotateAround (transform.position, transform.forward, rightAngle);
				}
			}


			//save the angle between oldPointL & newPointL, and oldPointR & newPointR
			//use crossproduct to determine if angle is positive or negative
			//if the two angles are opposites, do nothing
			//if the two angles are going in the same direction, set a threshold for how much they can differ
			//turn the wheel by the lowest angle if they differ too much, else change it by the average of the two angles





		} else {
			if (leftHandOn) {
				//kun venstre hånd

			
				Vector3 newPointL = new Vector3 ((transform.InverseTransformPoint (leftHand.transform.position)).x, (transform.InverseTransformPoint (leftHand.transform.position)).y, (transform.InverseTransformPoint (wheelCenterL.transform.position)).z);
				wheelCenterL.transform.LookAt (transform.TransformPoint (newPointL));
			
				//			Debug.DrawLine (oldPointL, newPointL, Color.red);
				//			Debug.DrawLine (oldPointL, transform.InverseTransformPoint (wheelCenterL.transform.position));
				//			Debug.DrawLine (newPointL, transform.InverseTransformPoint (wheelCenterL.transform.position));

				//			Debug.Log ("wheelCenter local pos: " + transform.InverseTransformPoint (wheelCenterL.transform.position));
				//			Debug.Log ("oldPoint local pos: " + oldPointL);
				//			Debug.Log ("newPoint local: pos: " + newPointL);



				//Quaternion rotationQuat = Quaternion.AngleAxis (Vector3.Angle (newPointL - wheelCenterL.transform.localPosition, wheelCenterTop.transform.localPosition - wheelCenterL.transform.localPosition), Vector3.forward);


				/*			Debug.DrawLine (newPointL, transform.InverseTransformPoint (wheelCenterL.transform.position), Color.red);
				Debug.DrawLine (oldPointL, transform.InverseTransformPoint (wheelCenterL.transform.position), Color.blue);

				Debug.DrawLine (rotationQuat * (newPointL - wheelCenterL.transform.position), rotationQuat * (oldPointL - wheelCenterL.transform.position), Color.blue, 5f);
				Debug.DrawLine (newPointL, oldPointL, Color.yellow);


				//TransformPoint: Local to global
				//InverseTransformPoint: Global to local
				
				Debug.DrawLine (Vector3.zero, 100 * Vector3.Cross (oldPointL - transform.InverseTransformPoint (wheelCenterL.transform.position), newPointL - transform.InverseTransformPoint (wheelCenterL.transform.position)), Color.black);
				Debug.Log (Vector3.Cross (oldPointL - transform.InverseTransformPoint (wheelCenterL.transform.position), newPointL - transform.InverseTransformPoint (wheelCenterL.transform.position)));
*/

				if ((Vector3.Cross (oldPointL - transform.InverseTransformPoint (wheelCenterL.transform.position), newPointL - transform.InverseTransformPoint (wheelCenterL.transform.position))).z > 0) {
					turnSide = 1;
				} else { 
					turnSide = -1;
				}

				if (Vector3.Cross (newPointL - transform.InverseTransformPoint (wheelCenterL.transform.position), transform.InverseTransformPoint (wheelCenterTop.transform.position) - transform.InverseTransformPoint (wheelCenterL.transform.position)).z > 0) {
					servoSide = 1;
				} else {
					servoSide = -1;
				}

				transform.RotateAround (transform.position, transform.forward, turnSide * Vector3.Angle (oldPointL - transform.InverseTransformPoint (wheelCenterL.transform.position), newPointL - transform.InverseTransformPoint (wheelCenterL.transform.position)));


				/*	if (leftHand.transform.position.x < wheelCenterL.transform.position.x) {
					if ((rotationQuat * (newPointL - wheelCenterL.transform.position)).x > (rotationQuat * (oldPointL - wheelCenterL.transform.position)).x) {
						turnSide = 1;
						transform.RotateAround (transform.position, transform.up, Vector3.Angle ((newPointL - wheelCenterL.transform.position), (oldPointL - wheelCenterL.transform.position)));
					} else {
						turnSide = -1;
						this.transform.RotateAround (transform.position, transform.up, -Vector3.Angle ((newPointL - wheelCenterL.transform.position), (oldPointL - wheelCenterL.transform.position)));
					}
				} else {
					if ((rotationQuat * (newPointL - wheelCenterL.transform.position)).x > (rotationQuat * (oldPointL - wheelCenterL.transform.position)).x) {
						turnSide = -1;
						transform.RotateAround (transform.position, transform.up, -Vector3.Angle ((newPointL - wheelCenterL.transform.position), (oldPointL - wheelCenterL.transform.position)));
					} else {
						turnSide = 1;
						this.transform.RotateAround (transform.position, transform.up, Vector3.Angle ((newPointL - wheelCenterL.transform.position), (oldPointL - wheelCenterL.transform.position)));
					}
				}*/



				/*if (newPointL.x < oldPointL.x) {
					this.transform.RotateAround (transform.position, transform.up, Vector3.Angle ((newPointL - wheelCenterL.transform.position), (oldPointL - wheelCenterL.transform.position)));
					turnSide = 1;
				}
				if (newPointL.x > oldPointL.x) {
					this.transform.RotateAround (transform.position, transform.up, -Vector3.Angle ((newPointL - wheelCenterL.transform.position), (oldPointL - wheelCenterL.transform.position)));
					turnSide = -1;
				}
				if (newPointL.x < oldPointL.x && newPointL.y < wheelCenterL.transform.position.y) {
					this.transform.RotateAround (transform.position, transform.up, -Vector3.Angle ((newPointL - wheelCenterL.transform.position), (oldPointL - wheelCenterL.transform.position)));
					turnSide = -1;
				}
				if (newPointL.x > oldPointL.x && newPointL.y < wheelCenterL.transform.position.y) {
					this.transform.RotateAround (transform.position, transform.up, Vector3.Angle ((newPointL - wheelCenterL.transform.position), (oldPointL - wheelCenterL.transform.position)));
					turnSide = 1;
				}
				//	this.transform.Rotate (new Vector3 (0, Vector3.Dot (oldPointL, newPoint) / ((oldPointL - wheelCenterL.transform.position).magnitude * (newPoint - wheelCenterL.transform.position).magnitude), 0));
*/


		

			}
			if (rightHandOn) {
				//kun højre hånd
				Vector3 newPointR = new Vector3 (transform.InverseTransformPoint (rightHand.transform.position).x, transform.InverseTransformPoint (rightHand.transform.position).y, transform.InverseTransformPoint (wheelCenterR.transform.position).z);
				wheelCenterR.transform.LookAt (transform.TransformPoint (newPointR));
			

				/*if (newPointR.x < oldPointR.x && newPointR.y > wheelCenterR.transform.position.y) {
					this.transform.RotateAround (transform.position, transform.up, Vector3.Angle ((newPointR - wheelCenterR.transform.position), (oldPointR - wheelCenterR.transform.position)));
					turnSide = 1;
				}
				if (newPointR.x > oldPointR.x && newPointR.y > wheelCenterR.transform.position.y) {
					this.transform.RotateAround (transform.position, transform.up, -Vector3.Angle ((newPointR - wheelCenterR.transform.position), (oldPointR - wheelCenterR.transform.position)));
					turnSide = -1;
				}

				if (newPointR.x < oldPointR.x && newPointR.y < wheelCenterR.transform.position.y) {
					this.transform.RotateAround (transform.position, transform.up, -Vector3.Angle ((newPointR - wheelCenterR.transform.position), (oldPointR - wheelCenterR.transform.position)));
					turnSide = -1;
				}
				if (newPointR.x > oldPointR.x && newPointR.y < wheelCenterR.transform.position.y) {
					this.transform.RotateAround (transform.position, transform.up, Vector3.Angle ((newPointR - wheelCenterR.transform.position), (oldPointR - wheelCenterR.transform.position)));
					turnSide = 1;
				}
				//	this.transform.Rotate (new Vector3 (0, Vector3.Dot (oldPointL, newPoint) / ((oldPointL - wheelCenterL.transform.position).magnitude * (newPoint - wheelCenterL.transform.position).magnitude), 0));
*/
				
				if ((Vector3.Cross (oldPointR - transform.InverseTransformPoint (wheelCenterR.transform.position), newPointR - transform.InverseTransformPoint (wheelCenterR.transform.position))).z > 0) {
					turnSide = 1;
				} else { 
					turnSide = -1;
				}

				if (Vector3.Cross (newPointR - transform.InverseTransformPoint (wheelCenterR.transform.position), transform.InverseTransformPoint (wheelCenterTop.transform.position) - transform.InverseTransformPoint (wheelCenterR.transform.position)).z > 0) {
					servoSide = -1;
				} else {
					servoSide = 1;
				}
				
				transform.RotateAround (transform.position, transform.forward, turnSide * Vector3.Angle (oldPointR - transform.InverseTransformPoint (wheelCenterR.transform.position), newPointR - transform.InverseTransformPoint (wheelCenterR.transform.position)));


			} 
			//Hands off
			if (!leftHandOn && !rightHandOn) {

			

				if (realAngle < -1) {
					//this.transform.Rotate (new Vector3 (0, 50 * Time.deltaTime, 0));
					transform.RotateAround (transform.position, transform.forward, -(realAngle) * Time.deltaTime * 2);
					//turnSide = 1;
				} else
				//this.transform.Rotate (new Vector3 (0, -50 * Time.deltaTime, 0));
					transform.RotateAround (transform.position, transform.forward, -(realAngle) * Time.deltaTime * 2);
				//	turnSide = -1;
			}
		}
		oldPointL = new Vector3 ((transform.InverseTransformPoint (leftHand.transform.position)).x, (transform.InverseTransformPoint (leftHand.transform.position)).y, (transform.InverseTransformPoint (wheelCenterL.transform.position)).z);
		oldPointR = new Vector3 ((transform.InverseTransformPoint (rightHand.transform.position)).x, (transform.InverseTransformPoint (rightHand.transform.position)).y, (transform.InverseTransformPoint (wheelCenterR.transform.position)).z);
	}

}

