using UnityEngine;
using System.Collections;

public class button : MonoBehaviour
{
	public float interactRange;
	bool reachedLeft = false;
	bool reachedRight = false;
	bool grabbedLeft = false;
	bool grabbedRight = false;
	public float grabThresh;
	public GameObject leftHand;
	public GameObject rightHand;
	Rigidbody myBody; 
	Vector3 handMovementL;
	Vector3 handMovementR;
	Vector3 lastHandLPosition;
	Vector3 lastHandLPosition2;
	Vector3 lastHandLPosition3;
	Vector3 lastHandRPosition;
	Vector3 lastHandRPosition2;
	Vector3 lastHandRPosition3;
	public float coolDown = 10;
	public float coolLeft;
	Renderer renderer;
	
	// Use this for initialization
	public	void Start ()
	{
		Cursor.lockState = CursorLockMode.Locked;
		//	SixenseInput leftHand = SixenseInput.Controllers[0];
		//	SixenseInput rightHand = SixenseInput.Controllers[1];
		myBody = this.GetComponent<Rigidbody> ();
		coolLeft = 0;
		renderer = GetComponent<Renderer> ();
		
	}

	public virtual void buttonPressed ()
	{
		 
	}

	// Update is called once per frame
	public	void Update ()
	{
		
		if (grabbedLeft) {
			//handMovementL = (lastHandLPosition3 + lastHandLPosition2 + lastHandLPosition + leftHand.transform.position) / 3;
			handMovementL = lastHandLPosition - leftHand.transform.position;
			Debug.DrawLine (lastHandLPosition, leftHand.transform.position);
			lastHandLPosition3 = lastHandLPosition2;
			lastHandLPosition2 = lastHandLPosition;
			lastHandLPosition = leftHand.transform.position;
			

		}
		if (grabbedRight) {
			//handMovementR = (lastHandRPosition3 + lastHandRPosition2 + lastHandRPosition + rightHand.transform.position) / 3;
			handMovementR = lastHandRPosition - rightHand.transform.position;
			Debug.DrawLine (lastHandRPosition, rightHand.transform.position);
			lastHandRPosition3 = lastHandRPosition2;
			lastHandRPosition2 = lastHandRPosition;
			lastHandRPosition = rightHand.transform.position;

		}
		//Debug.Log ("Mouse is locked? " + Cursor.lockState);
		//Debug.Log ("Distance to box-lefthand: " + (this.transform.position - leftHand.transform.position).magnitude + ". Distance to box-righthand: " + (this.transform.position - rightHand.transform.position).magnitude);
		//	Debug.Log ("Lefthand speed vector: " + handMovementL + ". Righthand speed vector: " + handMovementR);
		
		//left hand
		if ((this.transform.position - leftHand.transform.position).magnitude < interactRange && SixenseInput.Controllers [0].Trigger <= grabThresh) {
			//
			reachedLeft = true;
			//	Debug.Log ("lefthand close to the box");
		}
		if ((this.transform.position - leftHand.transform.position).magnitude > interactRange + interactRange * 0.1 && reachedLeft == true) {
			reachedLeft = false;
			grabbedLeft = false;

			//	Debug.Log ("lefthand left the box");
		}
		if ((this.transform.position - leftHand.transform.position).magnitude < interactRange && 
			SixenseInput.Controllers [0].Trigger > grabThresh && 
			reachedLeft == true &&
			grabbedLeft == false) {
			//interact
			buttonPressed ();
			grabbedLeft = true;
			reachedLeft = false;
			//	Debug.Log ("lefthand presed the button");
		}
		if (SixenseInput.Controllers [0].Trigger < grabThresh && grabbedLeft == true) {
			grabbedLeft = false;

			//	Debug.Log ("lefthand let go of the box");
		}
		
		
		//right hand
		if ((this.transform.position - rightHand.transform.position).magnitude < interactRange && SixenseInput.Controllers [1].Trigger <= grabThresh) {
			//
			reachedRight = true;
			//	Debug.Log ("righthand close to the box");
		}
		if ((this.transform.position - rightHand.transform.position).magnitude > interactRange && reachedRight == true) {
			reachedRight = false;
			grabbedRight = false;
			//	this.transform.parent = null;
			//		Debug.Log ("righthand left the box");
		}
		if ((this.transform.position - rightHand.transform.position).magnitude < interactRange && 
			SixenseInput.Controllers [1].Trigger > grabThresh && 
			reachedRight == true &&
			grabbedRight == false) {
			//interact
			buttonPressed ();
			//	this.transform.SetParent (rightHand.transform);
			grabbedRight = true;

//			Debug.Log ("righthand pressed the button!");
		}
		if (SixenseInput.Controllers [1].Trigger < grabThresh && grabbedRight == true) {
			grabbedRight = false;
			reachedRight = false;

			//		Debug.Log ("righthand let go of the box");
		}

		if (coolLeft > 0) {
			renderer.material.color = new Color (255, 0, 0);
			coolLeft -= Time.deltaTime;
		} else {
			renderer.material.color = new Color (0, 255, 0);
		}
	}
}
