using UnityEngine;
using System.Collections;

public class grabbable : MonoBehaviour
{
	public float interactRange;
	bool reachedLeft = false;
	bool reachedRight = false;
	bool grabbedLeft = false;
	bool grabbedRight = false;
	public float grabThresh;
	public GameObject leftHand;
	public GameObject rightHand;
	Rigidbody myBody; 
	Vector3 handMovementL;
	Vector3 handMovementR;
	Vector3 lastHandLPosition;
	Vector3 lastHandLPosition2;
	Vector3 lastHandLPosition3;
	Vector3 lastHandRPosition;
	Vector3 lastHandRPosition2;
	Vector3 lastHandRPosition3;

	// Use this for initialization
	void Start ()
	{
		Cursor.lockState = CursorLockMode.Locked;
		//	SixenseInput leftHand = SixenseInput.Controllers[0];
		//	SixenseInput rightHand = SixenseInput.Controllers[1];
		myBody = this.GetComponent<Rigidbody> ();

	}
	
	// Update is called once per frame
	void Update ()
	{

		if (grabbedLeft) {
			//handMovementL = (lastHandLPosition3 + lastHandLPosition2 + lastHandLPosition + leftHand.transform.position) / 3;
			handMovementL = lastHandLPosition - leftHand.transform.position;
			Debug.DrawLine (lastHandLPosition, leftHand.transform.position);
			lastHandLPosition3 = lastHandLPosition2;
			lastHandLPosition2 = lastHandLPosition;
			lastHandLPosition = leftHand.transform.position;

			this.transform.position = leftHand.transform.position;
		}
		if (grabbedRight) {
			//handMovementR = (lastHandRPosition3 + lastHandRPosition2 + lastHandRPosition + rightHand.transform.position) / 3;
			handMovementR = lastHandRPosition - rightHand.transform.position;
			Debug.DrawLine (lastHandRPosition, rightHand.transform.position);
			lastHandRPosition3 = lastHandRPosition2;
			lastHandRPosition2 = lastHandRPosition;
			lastHandRPosition = rightHand.transform.position;
			this.transform.position = rightHand.transform.position;
		}
		//Debug.Log ("Mouse is locked? " + Cursor.lockState);
		//Debug.Log ("Distance to box-lefthand: " + (this.transform.position - leftHand.transform.position).magnitude + ". Distance to box-righthand: " + (this.transform.position - rightHand.transform.position).magnitude);
		//	Debug.Log ("Lefthand speed vector: " + handMovementL + ". Righthand speed vector: " + handMovementR);

		//left hand
		if ((this.transform.position - leftHand.transform.position).magnitude < interactRange && SixenseInput.Controllers [0].Trigger <= grabThresh) {
			//
			reachedLeft = true;
			//Debug.Log ("lefthand close to the box");
		}
		if ((this.transform.position - leftHand.transform.position).magnitude > interactRange + interactRange * 0.1 && reachedLeft == true) {
			reachedLeft = false;
			grabbedLeft = false;
			this.transform.parent = null;
			//	Debug.Log ("lefthand left the box");
		}
		if ((this.transform.position - leftHand.transform.position).magnitude < interactRange && 
			SixenseInput.Controllers [0].Trigger > grabThresh && 
			reachedLeft == true &&
			grabbedLeft == false) {
			//interact
			this.transform.position = leftHand.transform.position;
			//	this.transform.SetParent (leftHand.transform);
			//	Debug.Log (this);
			grabbedLeft = true;
			myBody.useGravity = false;
			//	Debug.Log ("lefthand grab the box!");
		}
		if (SixenseInput.Controllers [0].Trigger < grabThresh && grabbedLeft == true) {
			grabbedLeft = false;
			myBody.useGravity = true;
			myBody.AddForce (handMovementL * 5000);
			//	Debug.Log ("lefthand let go of the box");
		}


		//right hand
		if ((this.transform.position - rightHand.transform.position).magnitude < interactRange && SixenseInput.Controllers [1].Trigger <= grabThresh) {
			//
			reachedRight = true;
			//	Debug.Log ("righthand close to the box");
		}
		if ((this.transform.position - rightHand.transform.position).magnitude > interactRange && reachedRight == true) {
			reachedRight = false;
			grabbedRight = false;
			//	this.transform.parent = null;
			//		Debug.Log ("righthand left the box");
		}
		if ((this.transform.position - rightHand.transform.position).magnitude < interactRange && 
			SixenseInput.Controllers [1].Trigger > grabThresh && 
			reachedRight == true &&
			grabbedRight == false) {
			//interact
			this.transform.position = rightHand.transform.position;
			//	this.transform.SetParent (rightHand.transform);
			grabbedRight = true;
			myBody.useGravity = false;
			//	Debug.Log ("righthand grab the box!");
		}
		if (SixenseInput.Controllers [1].Trigger < grabThresh && grabbedRight == true) {
			grabbedRight = false;
			reachedRight = false;
			//	this.transform.parent = null;
			myBody.useGravity = true;
			myBody.AddForce (handMovementR * 5000);
			//		Debug.Log ("righthand let go of the box");
		}
	}
}
