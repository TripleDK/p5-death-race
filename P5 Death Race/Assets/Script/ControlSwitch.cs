﻿using UnityEngine;
using System.Collections;

public class ControlSwitch : MonoBehaviour {

    public bool useHydra = false;
    public static ControlSwitch Instance;
    
    // Register the singleton
    void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError("Multiple instances of SpecialEffectsHelper!");
        }

        Instance = this;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Control Switch") == true)
        {
            useHydra = !useHydra;
        }
	}
}
