﻿using UnityEngine;
using System.Collections;

public class turboButton : smashButton
{

	public GameObject car;
	public float speedBoost;
	public bool boostOn;
	public float duration;
	public 	float timeLeft;

	// Use this for initialization
	void Start ()
	{
		boostOn = false;
	}
	
	// Update is called once per frame

	public override void interaction ()
	{
		//add turbo
		Debug.Log ("TURBO!!");
		boostOn = true;
		timeLeft = duration;


	}

}
