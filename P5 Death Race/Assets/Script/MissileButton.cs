﻿using UnityEngine;
using System.Collections;

public class MissileButton : button
{

	public GameObject missilePrefab;
	public GameObject car;
	public Vector3 positionAdjustment;

	// Use this for initialization


	public override void buttonPressed ()
	{
		if (coolLeft <= 0) {
			coolLeft = coolDown;
			GameObject newMissile = Instantiate (missilePrefab, car.transform.TransformPoint (positionAdjustment), transform.rotation) as GameObject;

			newMissile.transform.RotateAround (newMissile.transform.position, transform.right, 90);
			newMissile.transform.RotateAround (newMissile.transform.position, transform.up, 180);
		}
	}

	// Update is called once per frame


}
