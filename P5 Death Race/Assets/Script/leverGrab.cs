﻿using UnityEngine;
using System.Collections;

public class leverGrab : MonoBehaviour
{

	public GameObject leverCol;
	public float maxAngle;
	public float minAngle;
	public float interactRange;
	public float grabThresh;
	public GameObject leftHand;
	public GameObject rightHand;
	public GameObject centerPos;
	Vector3 lastPos;
	public float value;
	public float localAngle;
	bool reachedLeft = false;
	bool reachedRight = false;
	bool grabbedRight = false;
	bool grabbedLeft = false;


	// Use this for initialization
	void Start ()
	{
		value = 0;
		localAngle = 0;
	}
	
	// Update is called once per frame
	void Update ()
	{

		//Debug.Log ("Lever's rotation is: " + transform.eulerAngles.x);
		//Debug.Log ("Lever look at:" + transform.TransformPoint ((transform.InverseTransformPoint (rightHand.transform.position)).x, (transform.InverseTransformPoint (rightHand.transform.position)).y, transform.localPosition.z));
		//	Debug.DrawLine (new Vector3 (0, 0, 0), transform.TransformPoint (leverCol.transform.localPosition.x, (transform.InverseTransformPoint (rightHand.transform.position)).y, (transform.InverseTransformPoint (rightHand.transform.position)).z));
		//	Debug.DrawLine (new Vector3 (0, 0, 0), new Vector3 (leverCol.transform.localPosition.x, rightHand.transform.position.y, rightHand.transform.position.z), Color.blue);


		if ((this.transform.position - leftHand.transform.position).magnitude < interactRange && SixenseInput.Controllers [0].Trigger <= grabThresh) {
			reachedLeft = true;

		}
		if ((this.transform.position - leftHand.transform.position).magnitude > interactRange + interactRange * 0.1 && reachedLeft == true) {
			reachedLeft = false;
			grabbedLeft = false;
		}

		if ((this.transform.position - rightHand.transform.position).magnitude < interactRange && SixenseInput.Controllers [1].Trigger <= grabThresh) {
			reachedRight = true;

		}
		if ((this.transform.position - rightHand.transform.position).magnitude > interactRange && reachedRight == true) {
			reachedRight = false;
			grabbedRight = false;
		}


		if (transform.localEulerAngles.x - 270 < maxAngle && transform.localEulerAngles.x - 270 > minAngle) { 
			//	Debug.Log ("Lever's localEulerAngles.x + 270 = :" + (transform.localEulerAngles.x) + ", distance between right hand and leverhandle: " + (rightHand.transform.localPosition - leverCol.transform.localPosition).magnitude
			//		+ ", angle between hand and centerpos: " + Vector3.Angle ((leftHand.transform.localPosition - transform.localPosition), (centerPos.transform.localPosition - transform.localPosition)));
			//Debug.Log ("Hand pos: " + rightHand.transform.localPosition + ", leverHandle pos: " + leverCol.transform.localPosition);

			//Left hand
			if ((leftHand.transform.position - leverCol.transform.position).magnitude < interactRange && 
				SixenseInput.Controllers [0].Trigger > grabThresh &&
				Vector3.Angle ((leftHand.transform.localPosition - transform.localPosition), (centerPos.transform.localPosition - transform.localPosition)) < maxAngle) {
			
				//	Debug.Log ("Left hand grabbing lever!");
				/*difficult*/
				//TransformPoint: Local to global
				//InverseTransformPoint: Global to local
				transform.LookAt (transform.TransformPoint (leverCol.transform.localPosition.x, (transform.InverseTransformPoint (leftHand.transform.position)).y, (transform.InverseTransformPoint (leftHand.transform.position)).z));

				/*difficult*/

				if (transform.InverseTransformPoint (leftHand.transform.position).z > transform.localPosition.z) {
					localAngle = Vector3.Angle ((transform.TransformPoint ((transform.localPosition).x, transform.InverseTransformPoint (leftHand.transform.position).y, transform.InverseTransformPoint (leftHand.transform.position).z)), (centerPos.transform.position - transform.position));
					value = localAngle / maxAngle;
				} else {
					localAngle = -Vector3.Angle ((transform.TransformPoint ((transform.localPosition).x, transform.InverseTransformPoint (leftHand.transform.position).y, transform.InverseTransformPoint (leftHand.transform.position).z)), (centerPos.transform.position - transform.position));
					value = -localAngle / maxAngle;
				}
			}

			//Right hand
			if ((rightHand.transform.position - leverCol.transform.position).magnitude < interactRange && 
				SixenseInput.Controllers [1].Trigger > grabThresh &&
				Vector3.Angle ((rightHand.transform.position - transform.position), (centerPos.transform.position - transform.position)) < maxAngle) {

				//	Debug.Log ("Right hand grabbing lever!");
				/*difficult*/

				//TransformPoint: Local to global
				//InverseTransformPoint: Global to local
				transform.LookAt (transform.TransformPoint (leverCol.transform.localPosition.x, (transform.InverseTransformPoint (rightHand.transform.position)).y, (transform.InverseTransformPoint (rightHand.transform.position)).z));
				/*difficult*/


				if ((centerPos.transform.InverseTransformPoint (rightHand.transform.position)).z > centerPos.transform.localPosition.z) {
					localAngle = Vector3.Angle ((transform.TransformPoint (transform.localPosition.x, (transform.InverseTransformPoint (rightHand.transform.position)).y, (transform.InverseTransformPoint (rightHand.transform.position)).z)) - transform.position, (centerPos.transform.position - transform.position));
					value = localAngle / maxAngle;
					//	Debug.Log ("Lever ahead!");
				} else {
					localAngle = -Vector3.Angle ((transform.TransformPoint (transform.localPosition.x, (transform.InverseTransformPoint (rightHand.transform.position)).y, (transform.InverseTransformPoint (rightHand.transform.position)).z)) - transform.position, (centerPos.transform.position - transform.position));
					value = -localAngle / minAngle;
					//	Debug.Log ("Lever behind!");
				}
		
			} 
			lastPos = leverCol.transform.localPosition;
		}
	}
}